/// <reference path="../typings/jquery/jquery.d.ts"/>
/// <reference path="../typings/angularjs/angular.d.ts"/>
var myApp = angular.module("myApp", ["myApp.service3", "ivh.treeview"]);
var textoBusqueda = "";

myApp.config(function (ivhTreeviewOptionsProvider) {
  ivhTreeviewOptionsProvider.set({
    defaultSelectedState: false,
    validate: false,
    expandToDepth: 1,
    useCheckboxes: false,
    labelAttribute: "@label",
    twistieExpandedTpl: "<img src='./img/toggle.png' />",
    twistieCollapsedTpl: "<img src='./img/expand.png' />",
    twistieLeafTpl: "<img src='./img/status.png' />",
  });
});

myApp.config(['$httpProvider', function ($httpProvider) {
  //initialize get if not there
  if (!$httpProvider.defaults.headers.get) {
    $httpProvider.defaults.headers.get = {};
  }

  // Answer edited to include suggestions from comments
  // because previous version of code introduced browser-related errors

  //disable IE ajax request caching
  $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
  // extra
  $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
  $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
}]);

myApp.filter("trim", function () {
  return function (value) {
    if (!angular.isString(value)) {
      return value;
    }

    return value.replace(/^\s+|\s+$/g, ""); // You could use .trim, but it's not going to work on IE<9
  };
});

myApp.directive("onFinishRender", [
  "$timeout",
  "$parse",
  function ($timeout, $parse) {
    return {
      restrict: "A",
      link: function (scope, element, attr) {
        if (scope.$last === true) {
          $timeout(function () {
            scope.$emit("ngRepeatFinished");
            if (!!attr.onFinishRender) {
              $parse(attr.onFinishRender)(scope);
            }
          });
        }
      },
    };
  },
]);

myApp.controller(
  "ctlRoot",

  function ($scope, $timeout, $filter, DataSource, XML_Detail) {
    DesactivarDetalle();
    GoTop();

    $scope.detailFilter = {
      valueId: "",
    };

    $scope.visibleProp = true;
    $scope.visibleNoFilter = false;

    $scope.showActivity = function (msg) {
      $timeout(function () {
        $scope.activityMessage = msg;
      });
    };

    $scope.changeCssClass = function ($event) {
      if ($event.target.attributes["ng-src"].value !== "./img/bullet.png") {
        if ($event.target.className.indexOf("cerrado") !== -1) {
          $event.target.className = "collapsed abierto";
        } else if ($event.target.className.indexOf("abierto") !== -1) {
          $event.target.className = "collapsed cerrado";
        }
      } else {
        $event.target.removeAttribute("ng-click");
      }
    };

    $scope.checkChildNodes = function (value, $event) {
      var hasIT = value.hasOwnProperty("IT");
      var hasEX1 = value.hasOwnProperty("EX1");

      if (!value.hasOwnProperty("C") && !hasIT && !hasEX1) {
        return "./img/bullet.png";
      }

      return "./img/expand.png";
    };

    $scope.checkChildNodesSete = function (value, $event) {
      if (!value.hasOwnProperty("SCD")) {
        return "./img/bullet.png";
      }

      return "./img/expand.png";
    };

    $scope.setLabelTree = function (valueI, valueD) {
      var textLabel = "";

      if (
        valueI === undefined ||
        valueI === null ||
        valueD === undefined ||
        valueD === null
      ) {
        return textLabel;
      }

      try {
        if (valueD.startsWith(valueI)) {
          textLabel = valueD;
        } else {
          textLabel = valueI + " - " + valueD;
        }
      } catch (error) {
        if (valueD.indexOf(valueI) === 0) {
          textLabel = valueD;
        } else {
          textLabel = valueI + " - " + valueD;
        }
      }

      return textLabel;
    };

    $scope.withoutDash = function (string) {
      if (string !== undefined) {
        while (string.indexOf(".") != -1) {
          string = string.replace(".", "-");
        }
      }

      return string;
    };

    $scope.withoutDash2 = function (string) {
      if (string !== undefined) {
        while (string.indexOf(".") != -1) {
          string = string.replace(".", "-");
        }
      }

      return "#" + string;
    };

    $scope.hideActivity = function () {
      $timeout(function () {
        $scope.activityMessage = "";
      }, 1000); // message will be visible at least 1 sec
    };

    $scope.showActivityDetail = function (msg) {
      $timeout(function () {
        $scope.activityMessageDetail = msg;
      });
    };

    $scope.hideActivityDetail = function () {
      $timeout(function () {
        $scope.activityMessageDetail = "";
      }, 1000); // message will be visible at least 1 sec
    };

    $scope.radioChange = function (s) {
      if (s.hasOwnProperty("N")) {
        $scope.detailFilter.valueId = s.N.I;
      } else {
        $scope.detailFilter.valueId = s;
      }
    };

    $scope.filterNodes = function (node) {
      // No filter, so return everything
      if (
        $scope.detailFilter === undefined ||
        $scope.detailFilter === null ||
        $scope.detailFilter.valueId === ""
      )
        return true;

      var matched = true;

      var lastChar;

      // Otherwise apply your matching logic
      if (node.hasOwnProperty("N")) {
        var nodeN = node.N;
        if (nodeN.hasOwnProperty("SeteChar")) {
          var nodeSeteChar = nodeN.SeteChar.replace('.', '');

          if (nodeSeteChar.length === 7) {
            lastChar = nodeSeteChar.substr(nodeSeteChar.length - 1);

            if (lastChar !== $scope.detailFilter.valueId) {
              matched = false;
            } else {
              matched = true;
            }
          }
        }
      }
      else if (!node.hasOwnProperty("N")) {
        angular.forEach(node.SCD, function (value, key) {
          if (value.hasOwnProperty('N')) {
            var nodeN = value.N;
            if (nodeN.hasOwnProperty('SeteChar')) {
              var nodeSeteChar = nodeN.SeteChar;

              if (nodeSeteChar.indexOf('XX') != -1) {
                // debugger;
                return $scope.filterNodesWildcard(nodeSeteChar);
              }
              else {
                // debugger;
                // alert(nodeSeteChar.length);
                if (nodeSeteChar !== undefined && nodeSeteChar.length === 7) {
                  lastChar = nodeSeteChar.substr(nodeSeteChar.length - 1);

                  if (lastChar !== $scope.detailFilter.valueId) {
                    matched = false;
                  }
                  else {
                    matched = true;
                  }
                }
              }
            }
          }
        });
      }

      // node.SCD[0].N

      // matched = true;

      return matched;
    };

    $scope.filterNodesWildcard = function (node) {
      //debugger;
      var nodeSeteChar = node.replace('.', '');

      if (nodeSeteChar !== undefined && nodeSeteChar.length === 7) {
        lastChar = nodeSeteChar.substr(nodeSeteChar.length - 1);

        if (lastChar !== $scope.detailFilter.valueId) {
          return false;
        }
        else {
          return true;
        }
      }
    };

    $scope.filterNodesOptions = function (node) {
      var isOptionToFilter = true;

      if (node.N.hasOwnProperty("SeteChar")) {
        isOptionToFilter = false;
      }

      return isOptionToFilter;
    };

    $scope.haveSeteCaracter = function (rootNode) {
      var showFilter = false;

      if (!rootNode.hasOwnProperty("noFilter")) {
        if (rootNode.hasOwnProperty("SCD")) {
          showFilter = true;
        } else {
          if (rootNode.hasOwnProperty("C")) {
            angular.forEach(rootNode.C, function (index) {
              if (index.hasOwnProperty("SCD")) {
                showFilter = true;
              } else if (index.hasOwnProperty("C")) {
                angular.forEach(index.C, function (index2) {
                  if (index2.hasOwnProperty("SCD")) {
                    showFilter = true;
                  }
                });
              }
            });
          }
        }
      }

      return showFilter;
    };

    $scope.filterComodin = function (node) {
      var srcImg = $scope.checkChildNodesSete(node);

      if (srcImg.indexOf("bullet") !== -1 && node.I.length > 7) {
        var lastChar = node.I.substr(node.I.length - 1);

        if (
          $scope.detailFilter === undefined ||
          $scope.detailFilter === null ||
          $scope.detailFilter.valueId === ""
        )
          return true;

        debugger;

        if (lastChar !== $scope.detailFilter.valueId) {
          return false;
        } else {
          return true;
        }
      } else {
        return true;
      }
    };

    $scope.showVisibleProp = function (rootNode) {
      var showVisibleProp = false;

      if (rootNode.hasOwnProperty("noFilter")) {
        showVisibleProp = true;
      }

      return showVisibleProp;
    };

    $scope.getSeteCharacterFilter = function (rootNode) {
      var seteCharFilter = false;

      if (rootNode.hasOwnProperty("SCD")) {
        seteCharFilter = rootNode.SCD;
      } else {
        if (rootNode.hasOwnProperty("C")) {
          angular.forEach(rootNode.C, function (index) {
            if (index.hasOwnProperty("SCD")) {
              seteCharFilter = index.SCD;
            } else if (index.hasOwnProperty("C")) {
              angular.forEach(index.C, function (index2) {
                if (index2.hasOwnProperty("SCD")) {
                  seteCharFilter = index2.SCD;
                }
              });
            }
          });
        }
      }

      return seteCharFilter;
    };

    //Evento click de los (códigos) en la parte derecha. Van a buscar al Tabular
    $(".divDetail").bind("click", ".click2", function (event) {
      var code = event.target.text;
      var codeOK = "";

      if (code == "") {
        return;
      }

      //Es nula
      if (typeof code === "undefined") return;

      //Es "Seguent >>"
      if (code.match("ent >>$")) return;

      if (event.target.className.indexOf("click2") != -1) {
        codeOK = code;
        var pointcode = code.indexOf(".");
        $("#txtRedirect")[0].value = code;

        while ($("#txtRedirect")[0].value.indexOf(".") != -1) {
          $("#txtRedirect")[0].value = $("#txtRedirect")[0].value.replace(
            ".",
            "-"
          );
        }

        if (pointcode > -1) {
          codeOK = code.substring(0, pointcode);
        }
      } else {
        var pointcode = code.indexOf(".");
        if (pointcode == -1) return;
        $("#txtRedirect")[0].value = code;

        while ($("#txtRedirect")[0].value.indexOf(".") != -1) {
          $("#txtRedirect")[0].value = $("#txtRedirect")[0].value.replace(
            ".",
            "-"
          );
        }

        codeOK = code.substring(0, pointcode);
      }

      if (codeOK == "") {
        return;
      }

      XML_Detail.get(codeOK, function (rest) {
        //Cuando llega el dato...
        $scope.dbTabular = rest;

        //Cambio subrayar amarillo palabras de búsqueda
        window.setTimeout(function () {
          $("html,body").animate(
            {
              scrollTop: $(".divDetail").offset().top - 180,
            },
            "slow"
          );
        }, 500);
      });

      event.preventDefault();
    });

    //Evento click de la izquierda hacia la derecha
    $scope.nodeClick = function (nodo) {
      if (nodo.hasOwnProperty("children")) return;

      var code = nodo["@label"];
      var codeClear = code.substring(0, 4);
      codeClear = $.trim(codeClear);

      XML_Detail.get(codeClear, function (rest) {
        $scope.dbTabular = rest;
        window.setTimeout(function () {
          $("html,body").animate(
            {
              scrollTop:
                $(".titulo").offset().top - ($(".titulo2").outerHeight() + 50),
            },
            "slow"
          );
        }, 500);
      });

      $scope.detailFilter = {
        valueId: "",
      };

      event.preventDefault();
    };

    $scope.nodeCopyCode = function (code) {
      if (!code.hasOwnProperty("C")) {
        if (!code.hasOwnProperty("SCD")) {
          var valueToMatch = code.I ? code.I : code;
          var regEx = /[a-zA-Z]\d{2}/;
          var result = valueToMatch.match(regEx);
          if (result === null) return;

          $scope.clipboard(valueToMatch);
        }
      }
    };

    $scope.clipboard = function (code) {
      var aux = document.createElement("input");
      aux.setAttribute("value", code);
      document.body.appendChild(aux);
      aux.select();
      document.execCommand("copy");
      document.body.removeChild(aux);

      //Mensaje
      $().toastmessage(
        "showSuccessToast",
        "<br/>Codi <b>" + code + "</b> copiat al porta-retalls correctament"
      );
    };

    $scope.hideActivity();

    setData = function (data) {
      $scope.showActivityDetail("Loading detail...");

      $scope.dbMain = data;
      $("#txtFilter").focus();
      $scope.hideActivityDetail();
      $(".spinner").fadeTo("slow", 0);
      $(".spinner").hide();
    };

    DataSource.get(setData);

    $scope.GetDetail = function ($event, name) {
      $scope.myFilterDetail = name.title;
    };

    $scope.expand = function (element, nodo) {
      element = angular.element(element);
      var newDirective = angular.element(
        "<ul ng-repeat='lv2 in lv1.C'><li class='liItem' ng-click='GetDetail($event, lv2)'><img src='./img/bullet2.png' /><b>{{lv2._I}}</b> - {{lv2._D}}</li></ul>"
      );
      element.append(newDirective);
      $compile(newDirective)($scope);
    };

    $("#txtFind").keyup(function () {
      if ($("#txtFind")[0].value == "") {
        setTimeout(function () {
          $scope.$apply(function () {
            DesactivarDetalle();
          });
        }, 500);
      }
    });

    function GetAccents(string) {
      var w = string;
      while (w.indexOf("*") != -1) w = w.replace("*", "");
      while (w.indexOf("?") != -1) w = w.replace("?", "");

      return w;
    }

    function DesactivarDetalle() {
      $("#divDetalle").fadeTo("slow", 0, function () { });
    }
    function ActivarDetalle() {
      $("#divDetalle").fadeTo("slow", 1, function () {
        // Animation complete.
      });
    }

    function MakeTree() {
      $(".tree li").each(function () {
        if ($(this).children("ul").length > 0) {
          $(this).addClass("parent");
        }
      });

      $(".tree li.parent a").click(function () {
        $(this)
          .parent()
          .parent()
          .parent()
          .parent()
          .parent()
          .toggleClass("active");
        $(this)
          .parent()
          .parent()
          .parent()
          .parent()
          .parent()
          .children("ul")
          .slideToggle("fast");

        if (
          $(this)
            .parent("td")
            .parent()
            .parent()
            .parent()
            .parent()
            .children("ul").length > 0
        ) {
          if ($(this).children().attr("src") == "./img/expand.png") {
            $(this).children().attr("src", "./img/toggle.png");
          } else {
            $(this).children().attr("src", "./img/expand.png");
          }
        }

        $(".clicked").removeClass("clicked");
        $(this).parent().parent().parent().parent().toggleClass("clicked");
      });

      $(".tree li").each(function () {
        $(this).toggleClass("active");
        $(this).children("ul").slideToggle("fast");
      });

      $("#all").click(function () {
        $(".tree li.parent a")
          .not(".except")
          .each(function () {
            $(this)
              .parent()
              .parent()
              .parent()
              .parent()
              .parent()
              .toggleClass("active");
            $(this)
              .parent()
              .parent()
              .parent()
              .parent()
              .parent()
              .children("ul")
              .slideToggle("fast");
          });
      });

      $(".tree li.parent a")
        .not(".except")
        .each(function () {
          $(this)
            .parent()
            .parent()
            .parent()
            .parent()
            .parent()
            .toggleClass("active");
          $(this)
            .parent()
            .parent()
            .parent()
            .parent()
            .parent()
            .children("ul")
            .hide();
        });

      $(".except")
        .not(".except")
        .each(function () {
          $(this)
            .parent()
            .parent()
            .parent()
            .parent()
            .parent()
            .addClass("active");
          $(this)
            .parent()
            .parent()
            .parent()
            .parent()
            .parent()
            .children("ul")
            .show();
        });

      //Si hay algo seleccionado, abro el li
      var selected = $("#txtRedirect")[0].value;

      if (selected) {
        $("#" + selected)
          .children("ul")
          .slideToggle("fast");
        $("#" + selected).addClass("active");
        $("#" + selected).addClass("clicked");

        //Si tiene decimales
        if (selected.indexOf("-") > 0) {
          var posDash = selected.indexOf("-");
          var long = selected.length;
          var dif = long - posDash;

          switch (dif) {
            //Tres decimales
            case 4:
              //Nivel 2
              $("#" + selected)
                .parent("ul")
                .slideToggle("fast");
              $("#" + selected)
                .parent("ul")
                .addClass("active");
              $("#" + selected)
                .parent("ul")
                .parent("li")
                .parent("ul")
                .slideToggle("fast");
              $("#" + selected)
                .parent("ul")
                .parent("li")
                .parent("ul")
                .addClass("active");
              break;

            //Dos decimales
            case 3:
              $("#" + selected)
                .parent("ul")
                .slideToggle("fast");
              $("#" + selected)
                .parent("ul")
                .addClass("active");
              break;

            //Un decimal
            case 2:
              $("#" + selected).addClass("active");
              break;
          }
        }
      }

      TreeViewImage();
    }

    function TreeViewImage() {
      $("li:has(ul):not(.except, .active) img").each(function () {
        if (
          $(this)
            .parent()
            .parent()
            .parent()
            .parent()
            .parent()
            .parent()
            .children("ul").length > 0
        ) {
          $(this).attr("src", "./img/expand.png");
        }
      });
    }

    function GoTop() {
      $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
          $("#backToTop").fadeIn("slow");
        } else {
          $("#backToTop").fadeOut("slow");
        }
      });

      $("#backToTop").click(function () {
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
      });
    }

    $scope.rootArray = [];

    $scope.GetArrayLevel = function (obj) {
      if (obj !== null && obj !== undefined) {
        return jQuery.makeArray(obj);
      }
    };

    $scope.GetArrayCodigos = function (str) {
      var res = new Array();
      var resSTR = str.match(/([A-Z])\d\d.?\d{0,3}-?/g);
      if (resSTR == null) return res;

      switch (resSTR.length) {
        case 0:
          return "";

          break;
        case 1:
          var val2 = resSTR[0];
          val2 = val2.replace(",", "");
          val2 = val2.replace(")", "");
          val2 = val2.replace("(", "");
          val2 = $.trim(val2);
          res.push(val2);

          return res;

          break;
        default:
          var i = 0;

          for (i = 0; i < resSTR.length; ++i) {
            var val = resSTR[i];
            val = val.replace(",", "");
            val = val.replace(")", "");
            val = val.replace("(", "");
            val = $.trim(val);
            res.push(val);
          }

          break;
      }

      return res;
    };

    $scope.GetArrayByComma = function (str) {
      if (str == null) {
        return "";
      }

      var ar = $.makeArray(str.split(","));
      return ar;
    };

    $scope.GetArrayByDash = function (str) {
      if (str == null) {
        return "";
      }

      var ar = $.makeArray(str.split("-"));
      return ar;
    };

    $scope.GetUrlFind = function (str) {
      var url = $(location).attr("href");
      var start = url.indexOf("zoom_query=");
      var end = url.indexOf("&");

      start += 11;
      var url2 = url.substring(0, start);
      url2 += str;
      var endStr = url.substring(end, end + (url.length - end));
      url2 += endStr;
      return url2;
    };

    $scope.GetTrim = function (str) {
      return $.trim(str);
    };

    $scope.GetClear = function (str) {
      var v = str;
      while (v.indexOf(".-)") != -1) v = v.replace(".-)", ")");
      while (v.indexOf("-)") != -1) v = v.replace("-)", ")");
      while (v.indexOf("-, ") != -1) v = v.replace("-, ", ", ");

      var regExpIndex = new RegExp("[A-Za-z]{1}[0-9]{2}", "igm");
      v = v.replace(
        regExpIndex,
        "<a href='#' class='click2 tooltip3'>Codi</a>"
      );

      return v;
    };

    $scope.GetClearNombre = function (str) {
      var v = str;
      while (v.indexOf(".-)") != -1) v = v.replace(".-)", ")");
      while (v.indexOf("-)") != -1) v = v.replace("-)", ")");
      while (v.indexOf("-, ") != -1) v = v.replace("-, ", ", ");

      return v.split("(")[0];
    };

    $scope.GetClearNombre2 = function (str) {
      var resSTR = str.match(/\(([A-Z])\d\d.?\d{0,3}-?/g);

      if (resSTR == null) {
        return str;
      }

      if (resSTR.indexOf(",") == -1) {
        str = str.substring(0, str.indexOf(resSTR));
        return str;
      }

      str = str.substring(0, resSTR.indexOf(",")[0]);

      return str;
    };

    $scope.GetClearCodigo = function (str) {
      var v = str;
      while (v.indexOf(".-)") != -1) v = v.replace(".-)", ")");
      while (v.indexOf("-)") != -1) v = v.replace("-)", ")");
      while (v.indexOf("-, ") != -1) v = v.replace("-, ", ", ");

      return v.split("(")[1].replace(")", "");
    };

    $scope.IsGetClear = function (str) {
      var v = str;
      while (v.indexOf(".-)") != -1) v = v.replace(".-)", ")");
      while (v.indexOf("-)") != -1) v = v.replace("-)", ")");
      while (v.indexOf("-, ") != -1) v = v.replace("-, ", ", ");

      var vOld = v;
      var regExpIndex = new RegExp("[A-Za-z]{1}[0-9]{2}", "igm");
      v = v.replace(
        regExpIndex,
        "<a href='#' class='click2 tooltip3'>Codi</a>"
      );

      return v != vOld;
    };

    $scope.GetClear2 = function (str) {
      var v = str;
      v = v.replace("-", "");
      return v;
    };
  }
);

angular.module("myApp.service3", []).factory("DataSource", [
  "$http",
  function ($http) {
    return {
      get: function (callback) {
        $http.get("index2.json").success(function (data, status) {
          callback(data);
        });
      },
    };
  },
]);

myApp.factory("XML_Detail", function ($http) {
  return {
    get: function (code, callback2) {
      $http
        .get("./res/json/" + code + ".json")
        .success(function (data, status) {
          callback2(data);
        });
    },
  };
});
