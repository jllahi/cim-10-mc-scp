/// <reference path="../typings/jquery/jquery.d.ts"/>
/// <reference path="../typings/angularjs/angular.d.ts"/>
var myApp = angular.module('myApp', ['myApp.service3', 'ivh.treeview']);
var textoBusqueda = "";

myApp.config(function (ivhTreeviewOptionsProvider) {
    ivhTreeviewOptionsProvider.set({
        defaultSelectedState: false,
        validate: false,
        expandToDepth: 1,
        useCheckboxes: false,
        labelAttribute: '@label',
        twistieExpandedTpl: "<img src='./img/toggle.png' />",
        twistieCollapsedTpl: "<img src='./img/expand.png' />",
        twistieLeafTpl: "<img src='./img/status.png' />"
    });
});

myApp.controller('ctlRoot',

    function ($scope, $timeout, $filter, DataSource, XML_Detail, ivhTreeviewMgr) {
        $scope.txtFilter = '';
        $scope.selectedCode = '';
        $scope.finishCode = '';

        DesactivarDetalle();
        GoTop();

        //Test
        //$('#zoom_searchbox')[0].value = getParam("zoom_query");

        //Añadir código
        /*var codis=$('.result_title');                          
        codis.each(function( index ) {                      
             var href=codis[index].children[1];
             var code=href.getAttribute('href');                                       
             code=code.replace('/','');
             code=code.replace('.html','');                                     
             $( this ).append("(<b>"+code+"</b>)");
        });  
        */
        $scope.showActivity = function (msg) {
            $timeout(function () {
                $scope.activityMessage = msg;
            });
        };
        $scope.hideActivity = function () {
            $timeout(function () {
                $scope.activityMessage = '';
            }, 1000); // message will be visible at least 1 sec
        };

        $scope.showActivityDetail = function (msg) {
            $timeout(function () {
                $scope.activityMessageDetail = msg;
            });
        };
        $scope.hideActivityDetail = function () {
            $timeout(function () {
                $scope.activityMessageDetail = '';
            }, 1000); // message will be visible at least 1 sec
        };

        $('.results').bind('click', function (event)
        //$scope.codeClick = function (event) 
        {
            event.stopPropagation();
            event.preventDefault();
            var code = event.target.href;
            var codeOK = "";

            if (code == '') return;

            //Es nula
            if (typeof code === 'undefined') {
                code = event.toElement.parentElement.href;
                if (typeof code === 'undefined') return;
            }

            //Es "Seguent >>"
            if (code.match("ent >>$")) return;
            //Paginación
            if (code.match("zoom_query=")) return;


            var pointcode = code.lastIndexOf('/');
            pointcode++;
            codeOK = code.substring(pointcode, code.length);
            codeOK = codeOK.replace('.html', '');

            //codeOK=code.replace('.html','');
            //var pointcode=codeOK.lastIndexOf('.');
            //pointcode++;
            //codeOK=codeOK.substring(pointcode,code.length);
            //codeOK=codeOK.replace('.html','');

            if (codeOK == '') return;
            $('.txtCerca')[0].value = codeOK;
            $scope.findByCode();
            return false;
        });

        //Evento click de los (códigos) en la parte derecha. Van a buscar al Tabular
        $('.divDetail').bind('click', ".click2", function (event)
        //$scope.codeClick = function (event) 
        {
            var code = event.target.text;
            var codeOK = "";

            if (code == '') return;

            //Es nula
            if (typeof code === 'undefined') return;

            //Es "Seguent >>"
            if (code.match("ent >>$")) return;

            if (event.target.className.indexOf("click2") != -1) {
                codeOK = code;
                var pointcode = code.indexOf('.');
                $('#txtRedirect')[0].value = code;
                while ($('#txtRedirect')[0].value.indexOf('.') != -1) $('#txtRedirect')[0].value = $('#txtRedirect')[0].value.replace('.', '-');
                if (pointcode > -1) {
                    codeOK = code.substring(0, pointcode);
                }
            }
            else {
                var pointcode = code.indexOf('.');
                if (pointcode == -1) return;
                $('#txtRedirect')[0].value = code;
                while ($('#txtRedirect')[0].value.indexOf('.') != -1) $('#txtRedirect')[0].value = $('#txtRedirect')[0].value.replace('.', '-');
                codeOK = code.substring(0, pointcode);
            }

            if (codeOK == '') return;


            //$(".divDetail2").hide();
            //$(".divDetailExt2").hide();
            //$(".divDetailExt").hide();
            //$(".divDetail").hide();  
            // $(".divDetail2").fadeTo( "fast" , 0);
            //$(".divDetailExt2").fadeTo( "fast" , 0);
            //$(".divDetailExt").fadeTo( "fast" , 0);
            //$(".divDetail").fadeTo( "fast" , 0);

            XML_Detail.get(codeOK, function (rest) {
                //Cambio subrayar amarillo palabras de búsqueda                        
                window.setTimeout(function () {
                    //var val=getParam("zoom_query"); 
                    //var html2=$('.divDetail').html();                                                                                                                             
                    //var words = val.split(" ");                                                                    
                    //$(".divDetail").hide();           
                    //$('.divDetail2').html(html2);
                    //MarcarAmarillo();                          
                    //$(".divDetail2").fadeTo( "fast" , 1 );  
                    $('html,body').animate(
                        {
                            scrollTop: $('.divDetail').offset().top - 180
                        }, 'slow');
                    //MakeTree();
                }, 500);

            });
            event.preventDefault();
        });

        $scope.radioClick = function (nodo, codi, id) {
            //Saco el TD
            var td = jQuery($('#' + id).parent().parent()[0]);
            var tr = jQuery(td.parent());
            var idTr = tr.attr('num');
            //var table=jQuery(td.parent().parent());
            //Deselecciono todos los items del mismo TD                                          
            $scope.desSeleccionarTD(td);
            //Selecciono el item            
            $scope.seleccionarItem(id);
            //Escribo el código
            $scope.escribirCode(td);
            //Habilito el siguiente TD 
            $scope.habilitarSiguienteTD(td);
            //Descheckear radiobuttons de otros TR
            jQuery($('#tbMaster tbody').children().not('[num=' + idTr + ']').children().children().children().attr('checked', false));
            //Deshabiltiar radiobuttons de otros TR
            jQuery($('#tbMaster tbody').children().not('[num=' + idTr + ']').children().not('[num=td-0]').children().children().attr('disabled', true));
            //Quitar azul de otros TR            
            jQuery($('#tbMaster tbody').children().not('[num=' + idTr + ']').children().children().removeClass('divSelected').addClass("divNoSelected"));
        };

        $scope.seleccionarItem = function (id) {
            jQuery($('#' + id).parent().removeClass("divNoSelected").addClass("divSelected"));
        };

        $scope.habilitarTD = function (td) {
            jQuery(td.children().children().attr('disabled', false));
        };

        $scope.habilitarSiguienteTD = function (td) {
            var next = jQuery(td.next());
            var tr = jQuery(td.parent());
            var last = jQuery(tr.children().last());

            var num1 = td.attr('num');
            var num2 = last.attr('num');

            //Si todos los TD tienen un elemento seleccionado enseñamos el código
            var numTd = jQuery($('#tbMaster tbody tr:first').children().length);
            var numSelecteds = jQuery($(tr).find('.divSelected').children().length);

            if (numTd[0] != numSelecteds[0]) {
                $scope.habilitarTD(next);
                $scope.NoMostrarCodeFinal();
            }
            else {
                /*if(num1==num2)
                {
                    $scope.MostrarCodeFinal();
                    $scope.clipboard();
                }*/
            }

            //Compruebo que el siguiente TD. Si sólo tiene un elemento, lo activo y marco y continuo con el siguiente..
            if (next.children().length != 1) {
                if (numTd[0] == numSelecteds[0]) {
                    $scope.MostrarCodeFinal();
                    $scope.clipboard();
                }
                return;
            }

            var nodo = jQuery(next.children()[0]);
            var rad = jQuery(nodo.children()[0]);
            var code = rad.attr('value');
            var id = rad.attr('id');
            jQuery(rad.attr('checked', true));
            jQuery(rad.attr('disabled', false));
            $scope.radioClick(nodo, code, id);
        };

        $scope.MostrarCodeFinal = function () {
            $('.divCode').show(500);
        };

        $scope.NoMostrarCodeFinal = function () {
            $('.divCode').hide(100);
        };

        $scope.deshabilitarTD = function (td) {
            jQuery(td.children().disabled = "true");
            jQuery(td.children().removeClass("divSelected").addClass("divNoSelected"));
        };

        $scope.deshabilitarTodosTDs = function () {
            jQuery($('#tbMaster tbody').children().children().not('[num=td-0]').children().children().attr('disabled', true));
        }

        $scope.desSeleccionarTD = function (td) {
            jQuery(td.children().removeClass("divSelected").addClass("divNoSelected"));
        };

        $scope.escribirCode = function (td) {
            $scope.finishCode = $scope.selectedCode;
            //Recojo el TR
            var tr = jQuery(td[0].parentNode)[0];
            //Por cada TD recojo su valor
            jQuery(
                $(tr).find('td').each(function () {
                    //Si no han acabado de construir el código marchamos
                    if ($(this).find('.divSelected').count == 0) return;

                    var codeTD = $(this).find('.divSelected').children().val();
                    if (codeTD != null) $scope.finishCode += codeTD;
                }
                )
            );
        }

        $scope.ObtenerName = function (pcsTables, row, axis) {
            var num1 = $scope.GetArrayLevel(pcsTables.pcsRow).indexOf(row);
            var num2 = $scope.GetArrayLevel(row.axis).indexOf(axis);
            return String(num1) + String(num2);
        }

        $scope.ObtenerID = function (pcsTables, row, axis, label) {
            var num1 = $scope.GetArrayLevel(pcsTables.pcsRow).indexOf(row);
            var num2 = $scope.GetArrayLevel(row.axis).indexOf(axis);
            var num3 = $scope.GetArrayLevel(axis.label).indexOf(label);
            return String(num1) + String(num2) + String(num3);
        }

        //Evento click de la izquierda hacia la derecha
        $scope.nodeClick = function (nodo) {
            if (nodo.hasOwnProperty('children')) return;

            //$(".divDetail").fadeTo( "fast" , 0);        

            var code = nodo['@label'];
            var codeClear = code.substring(0, 4);
            codeClear = $.trim(codeClear);

            //$(".divDetail2").hide();            

            XML_Detail.get(codeClear, function (rest) {
                //Cuando llega el dato...
                $scope.selectedCode = codeClear;
                $scope.finishCode = codeClear;

                $scope.NoMostrarCodeFinal();

                $scope.dbTabular = rest;
                window.setTimeout(function () {
                    $scope.deshabilitarTodosTDs();

                    $('html,body').animate(
                        {
                            scrollTop: $('.titulo').offset().top - ($('.titulo2').outerHeight() + 50)
                        }, 'slow');
                }, 500);

            });

            event.preventDefault();
        };



        $scope.clipboard = function () {
            var aux = document.createElement("input");
            var code = $scope.finishCode;
            aux.setAttribute("value", code);
            document.body.appendChild(aux);
            aux.select();
            document.execCommand("copy");
            document.body.removeChild(aux);

            //Mensaje
            $().toastmessage('showSuccessToast', "<br/>Codi <b>" + $scope.finishCode + "</b> copiat al porta-retalls correctament");
        };

        $scope.keyEnter = function (keyEvent) {
            if (keyEvent.which === 13)
                $scope.findByCode();
        }

        $scope.Expand = function () {
            jQuery($('.results').slideToggle('slow'));
            jQuery($('.result_pages').slideToggle('slow'));

            var _this = $('.expand')[0].children[0];
            //var current = _this.attr("src");
            if (_this.src.match("/CatSalut/img/expand2.png")) _this.src = "/CatSalut/img/expand1.png";
            else _this.src = "/CatSalut/img/expand2.png";
        }

        $scope.findByCode = function () {
            var txtCode = $('.txtCerca')[0].value.toUpperCase();
            // debugger;

            if (txtCode.length < 3) {
                $().toastmessage('showErrorToast', "<br/>Has d&#180;indicar un codi d&#180;una longitud m&iacute;nima de tres car&agrave;cters");
                //swal('Informacio', 'Has d indicar un codi d una longitud mínima de 3 caràcters', 'error');                
                return;
            }

            var codeClear = txtCode.substring(0, 3)

            XML_Detail.get(codeClear, function (rest) {
                //Cuando llega el dato...
                $scope.selectedCode = codeClear;
                $scope.finishCode = codeClear;


                $scope.dbTabular = rest;
                window.setTimeout(function () {
                    $scope.selectRemainder(txtCode);
                    $scope.finishCode = txtCode;
                    $('html,body').animate(
                        { scrollTop: $('.divSelected').offset().top - ($('.divSelected').outerHeight() + 100) }, 'slow');

                }, 500);

            });

            event.preventDefault();
        };


        // $scope.showActivity('Loading items...');

        $scope.hideActivity();

        //$scope.showActivity('Loading...');
        setData = function (data) {
            $scope.showActivityDetail("Loading detail...");
            $scope.dbMain = data;
            $('#txtFilter').focus();
            $scope.hideActivityDetail();
            $(".spinner").fadeTo("slow", 0);
            $(".spinner").hide();
        }



        DataSource.get(setData);


        $scope.selectRemainder = function (code) {
            //Deshabilitar todos los TDs
            $scope.deshabilitarTodosTDs();

            //Seleccionar
            var codeRemainder = code.substring(3, code.length);

            if (codeRemainder == '') {
                return;
            }

            var i = 0;

            var tr;

            // debugger;

            switch (codeRemainder.length) {
                case 2:
                    tr = jQuery($('#tbMaster tbody tr').has('td[num=td-0] input[value=' + codeRemainder[0] + ']').has('td[num=td-1] input[value=' + codeRemainder[1] + ']'));
                    break;
                case 3:
                    tr = jQuery($('#tbMaster tbody tr').has('td[num=td-0] input[value=' + codeRemainder[0] + ']').has('td[num=td-1] input[value=' + codeRemainder[1] + ']').has('td[num=td-2] input[value=' + codeRemainder[2] + ']'));
                    break;
                case 4:
                    tr = jQuery($('#tbMaster tbody tr').has('td[num=td-0] input[value=' + codeRemainder[0] + ']').has('td[num=td-1] input[value=' + codeRemainder[1] + ']').has('td[num=td-2] input[value=' + codeRemainder[2] + ']').has('td[num=td-3] input[value=' + codeRemainder[3] + ']'));
                    break;
                default:
                    tr = jQuery($('#tbMaster tbody tr').has('td[num=td-0] input[value=' + codeRemainder[0] + ']'));
                    break;
            }

            //var tr=jQuery($('#tbMaster tbody tr').has('td[num=td-0] input[value='+codeRemainder[0]+']').has('td[num=td-1] input[value='+codeRemainder[1]+']').has('td[num=td-2] input[value='+codeRemainder[2]+']').has('td[num=td-3] input[value='+codeRemainder[3]+']'));            
            // var tr = jQuery($('#tbMaster tbody tr').has('td[num=td-0] input[value=' + codeRemainder[0] + ']').has('td[num=td-1] input[value=' + codeRemainder[1] + ']'));//.has('td[num=td-2] input[value='+codeRemainder[2]+']').has('td[num=td-3] input[value='+codeRemainder[3]+']'));            
            // var tr = jQuery($('#tbMaster tbody tr').has('td[num=td-0] input[value=' + codeRemainder[0] + ']').has('td[num=td-1] input[value=' + codeRemainder[1] + ']').has('td[num=td-2] input[value=' + codeRemainder[2] + ']').has('td[num=td-3] input[value=' + codeRemainder[3] + ']'));
            // debugger;

            for (i = 0; i < codeRemainder.length; i++) {
                var char = codeRemainder[i];
                var radio = jQuery($(tr).find('td[num=td-' + i + ']').find('input[value="' + char + '"]'))[0];
                radio.checked = true;
                var id = radio.id;
                jQuery($('#' + id).parent().addClass("divSelected"));
                //Habilito TD
                $scope.habilitarTD(jQuery($('#' + id).parent().parent()));
            }

            if (code.length < 7) {
                var td = jQuery($('#' + id).parent().parent());
                $scope.habilitarSiguienteTD(td);
            }
            else {
                debugger;
                $scope.finishCode = code;
                $scope.clipboard();
            }
        }

        $scope.BetterTR = function (codeRemainder) {
            var i = 0;
            var rows = jQuery($('#tbMaster tbody tr'));
            for (tr = 0; tr < rows.length; i++) {
                var contain = 0;
                var td = 0;
                for (td = 0; td < rows[tr].children.length; td++) {
                    var divs = rows[tr].children[td].children;
                    //.has('input[value='+codeRemainder[td]+']');
                    var div = 0;
                    for (div = 0; div < divs.length; div++) {
                        var input = 0;
                        var inputs = divs[div].children;
                        for (input = 0; input < inputs.length; input++) {
                            var id = inputs[input].id;
                            if ($('#' + id).val() == codeRemainder[td]) {
                                contain++;
                            }

                        }
                    }
                }
                if (contain == 4) {
                    return rows[i];
                }
            };
        }


        $scope.GetDetail = function ($event, name) {
            $scope.myFilterDetail = name.title;
        }

        $scope.expand = function (element, nodo) {
            element = angular.element(element);
            var newDirective = angular.element("<ul ng-repeat='lv2 in lv1.C'><li class='liItem' ng-click='GetDetail($event, lv2)'><img src='./img/bullet2.png' /><b>{{lv2._I}}</b> - {{lv2._D}}</li></ul>");
            element.append(newDirective);
            $compile(newDirective)($scope);

        }


        $('#txtFind').keyup(function () {
            if ($('#txtFind')[0].value == '') {
                setTimeout(function () {
                    $scope.$apply(function () {
                        DesactivarDetalle();
                    });
                }, 500);
            }
        });

        function GetAccents(string) {
            var w = string;
            while (w.indexOf('*') != -1) w = w.replace('*', '');
            while (w.indexOf('?') != -1) w = w.replace('?', '');

            return w;
        }


        function DesactivarDetalle() {
            $("#divDetalle").fadeTo("slow", 0, function () {
                //$scope.dbTabular=null;
            });
        }
        function ActivarDetalle() {
            $("#divDetalle").fadeTo("slow", 1, function () {
                // Animation complete.
            });
        }

        function MakeTree() {
            $('.tree li').each(function () {
                if ($(this).children('ul').length > 0) {
                    $(this).addClass('parent');
                }
            });

            $('.tree li.parent a').click(function () {
                $(this).parent().parent().parent().parent().parent().toggleClass('active');
                $(this).parent().parent().parent().parent().parent().children('ul').slideToggle('fast');
                if ($(this).parent('td').parent().parent().parent().parent().children('ul').length > 0) {
                    if ($(this).children().attr("src") == "./img/expand.png") {
                        $(this).children().attr("src", "./img/toggle.png");
                    }
                    else {
                        $(this).children().attr("src", "./img/expand.png");
                    }
                }

                $('.clicked').removeClass('clicked');
                $(this).parent().parent().parent().parent().toggleClass('clicked');
            });

            $('.tree li').each(function () {
                $(this).toggleClass('active');
                $(this).children('ul').slideToggle('fast');
            });

            $('#all').click(function () {
                $('.tree li.parent a').not('.except').each(function () {
                    $(this).parent().parent().parent().parent().parent().toggleClass('active');
                    $(this).parent().parent().parent().parent().parent().children('ul').slideToggle('fast');
                });
            });

            // $( '#all' ).click( function() {					
            $('.tree li.parent a').not('.except').each(function () {
                $(this).parent().parent().parent().parent().parent().toggleClass('active');
                $(this).parent().parent().parent().parent().parent().children('ul').hide();
            });

            $('.except').not('.except').each(function () {
                $(this).parent().parent().parent().parent().parent().addClass('active');
                $(this).parent().parent().parent().parent().parent().children('ul').show();
            });
            //});

            //Si hay algo seleccionado, abro el li
            var selected = $('#txtRedirect')[0].value;
            if (selected) {
                $('#' + selected).children('ul').slideToggle('fast');
                $('#' + selected).addClass('active');
                $('#' + selected).addClass('clicked');

                //Si tiene decimales
                if (selected.indexOf('-') > 0) {
                    var posDash = selected.indexOf('-');
                    var long = selected.length;
                    var dif = long - posDash;
                    switch (dif) {
                        //Tres decimales
                        case 4:
                            //Nivel 2
                            $('#' + selected).parent('ul').slideToggle('fast');
                            $('#' + selected).parent('ul').addClass('active');
                            $('#' + selected).parent('ul').parent('li').parent('ul').slideToggle('fast');
                            $('#' + selected).parent('ul').parent('li').parent('ul').addClass('active');
                            break;
                        //Dos decimales
                        case 3:
                            $('#' + selected).parent('ul').slideToggle('fast');
                            $('#' + selected).parent('ul').addClass('active');
                            break;
                        //Un decimal
                        case 2:
                            $('#' + selected).addClass('active');
                            break;
                    }
                }
            }
            TreeViewImage();
        }

        function TreeViewImage() {
            $('li:has(ul):not(.except, .active) img').each(function () {
                if ($(this).parent().parent().parent().parent().parent().parent().children('ul').length > 0) {
                    $(this).attr("src", "./img/expand.png");
                }
                else {

                }
            });
        }

        function GoTop() {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 50) {
                    $('#backToTop').fadeIn('slow');
                } else {
                    $('#backToTop').fadeOut('slow');
                }
            });

            $('#backToTop').click(function () {
                $("html, body").animate({ scrollTop: 0 }, 500);
                return false;
            });
        }

        $scope.rootArray = [];
        $scope.GetArrayLevel = function (obj) {
            return jQuery.makeArray(obj)
        }

        $scope.GetArrayCodigos = function (str) {
            var res = new Array();
            var resSTR = str.match(/([A-Z])\d\d.?\d{0,3}-?/g);
            if (resSTR == null) return res;

            switch (resSTR.length) {
                case 0:
                    return '';
                    break;
                case 1:
                    var val2 = resSTR[0];
                    val2 = val2.replace('-', '');
                    val2 = val2.replace(',', '');
                    val2 = val2.replace(')', '');
                    val2 = val2.replace('(', '');
                    val2 = $.trim(val2);
                    res.push(val2);

                    return res;
                    break;
                default:
                    var i = 0;
                    for (i = 0; i < resSTR.length; ++i) {
                        var val = resSTR[i];
                        val = val.replace('-', '');
                        val = val.replace(',', '');
                        val = val.replace(')', '');
                        val = val.replace('(', '');
                        val = $.trim(val);
                        res.push(val);
                    }
                    break;
            }
            return res;
        }

        $scope.GetArrayByComma = function (str) {
            if (str == null) {
                return '';
            }

            var ar = $.makeArray(str.split(','));
            return ar;

        }

        $scope.GetArrayByDash = function (str) {
            if (str == null) return '';

            var ar = $.makeArray(str.split('-'));
            return ar;

        }

        $scope.GetUrlFind = function (str) {
            var url = $(location).attr('href');
            var start = url.indexOf("zoom_query=");
            var end = url.indexOf("&");

            start += 11;
            var url2 = url.substring(0, start);
            url2 += str;
            var endStr = url.substring(end, end + (url.length - end));
            url2 += endStr;
            return url2
        }

        $scope.GetTrim = function (str) {
            return $.trim(str);
        }

        $scope.GetClear = function (str) {
            var v = str;
            while (v.indexOf('.-)') != -1) v = v.replace('.-)', ')');
            while (v.indexOf('-)') != -1) v = v.replace('-)', ')');
            while (v.indexOf('-, ') != -1) v = v.replace('-, ', ', ');

            var regExpIndex = new RegExp('[A-Za-z]{1}[0-9]{2}', "igm");
            v = v.replace(regExpIndex, "<a href='#' class='click2 tooltip3'>Codi</a>");

            return v;
        }

        $scope.GetClearNombre = function (str) {
            var v = str;
            while (v.indexOf('.-)') != -1) v = v.replace('.-)', ')');
            while (v.indexOf('-)') != -1) v = v.replace('-)', ')');
            while (v.indexOf('-, ') != -1) v = v.replace('-, ', ', ');

            return v.split('(')[0];
            var regExpIndex = new RegExp('[A-Za-z]{1}[0-9]{2}', "igm");
            v = v.replace(regExpIndex, "<a href='#' class='click2 tooltip3'>Codi</a>");

            return v;
        }

        $scope.GetClearNombre2 = function (str) {
            var resSTR = str.match(/\(([A-Z])\d\d.?\d{0,3}-?/g);
            if (resSTR == null) return str;
            if (resSTR.indexOf(',') == -1) {
                str = str.substring(0, str.indexOf(resSTR));
                return str;
            }

            str = str.substring(0, resSTR.indexOf(',')[0]);
            return str;
        }

        $scope.GetClearCodigo = function (str) {
            var v = str;
            while (v.indexOf('.-)') != -1) v = v.replace('.-)', ')');
            while (v.indexOf('-)') != -1) v = v.replace('-)', ')');
            while (v.indexOf('-, ') != -1) v = v.replace('-, ', ', ');

            return v.split('(')[1].replace(")", "");
            var regExpIndex = new RegExp('[A-Za-z]{1}[0-9]{2}', "igm");
            v = v.replace(regExpIndex, "<a href='#' class='click2 tooltip3'>Codi</a>");

            return v;
        }

        $scope.IsGetClear = function (str) {
            var v = str;
            while (v.indexOf('.-)') != -1) v = v.replace('.-)', ')');
            while (v.indexOf('-)') != -1) v = v.replace('-)', ')');
            while (v.indexOf('-, ') != -1) v = v.replace('-, ', ', ');

            var vOld = v;
            var regExpIndex = new RegExp('[A-Za-z]{1}[0-9]{2}', "igm");
            v = v.replace(regExpIndex, "<a href='#' class='click2 tooltip3'>Codi</a>");

            return v != vOld;
        }

        $scope.GetClear2 = function (str) {
            var v = str;
            v = v.replace('-', '');

            return v;
        }

    });

angular.module('myApp.service3', []).
    factory('DataSource', ['$http', function ($http) {

        return {
            get: function (callback) {
                $http.get(
                    'menu.json'
                ).
                    success(function (data, status) {
                        callback(data);
                    })
            }
        }
    }]);

myApp.factory('XML_Detail', function ($http) {
    return {
        get: function (code, callback2) {
            $http.get(
                './res/pcsTable/' + code + '.json'
            ).
                success(function (data, status) {
                    callback2(data);
                    $().toastmessage('showSuccessToast', "<br/>Carregat procediment <b>" + code + "</b>");
                }).error(function () {
                    $().toastmessage('showWarningToast', "<br/>No existeix cap procediment amb aquest codi");
                })

        }
    }
});






