/// <reference path="../typings/jquery/jquery.d.ts"/>
/// <reference path="../typings/angularjs/angular.d.ts"/>
var myApp = angular.module('myApp', ['myApp.service3']);
var codeOK="";
var codeClear="";
myApp.controller('ctlRoot',

    function ($scope, $timeout, $filter,DataSource,XML_Detail)
    {
		$("table a").click(function(event){			  
          event.stopPropagation();
          event.preventDefault();  
            
          codeOK=this.text.replace(/\r?\n|\r/g,'');
          if(codeOK.length==2) return;
          
          var pointcode=codeOK.indexOf('.');
          codeClear=codeOK.substring(0,pointcode);
          
          $scope.findByCode();   
	    });
        
        $scope.keyEnter = function(keyEvent) {
        if (keyEvent.which === 13)
            {
                swal({   title: "Carregant",   text: "Espereu un moment, si us plau. <br/> <img src='./images/carregant.gif' style='margin-top:20px;' />",   timer: 1000,   showConfirmButton: false,html: true });
                searchData();
                // var buscar=$("#txtBuscar").val();
                // $('tr').show();
                // $('tr').not('[val*='+buscar+']').hide();

            }
        }
        
        var found=false;     
        var codeOK2="";    
        $scope.findByCode = function()
        {
            var txtCode=codeClear;	
             codeOK2=codeOK.replace("-","");
            found=false;	            			         
            
            XML_Detail.get(txtCode,function(rest)
				 {                             
					$scope.dbTabular=rest;        
                    //Busco dentro del JSON 
                    var arr = jQuery.makeArray(rest.root);
                    var res=matchId(arr);  
                    if(found == false)
                    {
                        swal('Error', "No s'ha trobat cap descriptiu amb el codi "+codeOK, 'error');
                    }                                                                                            
				 });
            
			event.preventDefault();    
        };   
        
        
        
        function matchId(json){
            if (!(json && "object" === typeof json)) { return; }                        
            var c=jQuery.makeArray(json);
                        
            var i=0;                       
            if (c == undefined) return;
            for(i=0; i<c.length;i++)
            {
				var len = c[i].I.length;
				
				if (codeOK2.substring(0,len) != c[i].I)
					continue;
					
                if(c[i].I==codeOK2)
                { 
                    found=true;
                    swal({   title: "Codi "+codeOK,   text: c[i].D,   html: true, cancelButtonText: "Tanca",confirmButtonText: "Copia codi",showCancelButton: true,closeOnConfirm: false },
                    
                    function()
                    {   
                        var aux = document.createElement("input");
                        aux.setAttribute("value", codeOK);            
                        document.body.appendChild(aux);            
                        aux.select();            
                        document.execCommand("copy");            
                        document.body.removeChild(aux);
                        swal("Copiat!", "Codi "+codeOK+" copiat al porta-retalls.", "success");                         
                        return;
                    }
                    );
                    
                }
                else
                {
                    if (c[i].C !== undefined) matchId(jQuery.makeArray(c[i].C));                                               
                }
            }
            
            return;           
        };

        function searchData(){
            var buscar=$("#txtBuscar").val();
            $('tbody>tr').show();
            $('tbody>tr').not('[val*='+buscar+']').css("display","none");
            $('tbody>tr').is('[val*='+buscar+']').css("display","block");
            $('tbody>tr').is('[val*='+buscar+']').width('100%');
        };
        
         $("#btnCercar").click(function() {
                swal({   title: "Carregant",   text: "Espereu un moment, si us plau. <br/> <img src='./images/carregant.gif' style='margin-top:20px;' />",   timer: 1000,   showConfirmButton: false,html: true });
                searchData();
                // var buscar=$("#txtBuscar").val();
                // $('tr').show();
                // $('tr').not('[val*='+buscar+']').hide();
         });
         $("#btnTots").click(function() {
                $("#txtBuscar").val('');
                $('tr').show();		
         });	
    });

angular.module('myApp.service3',[]).
    factory('DataSource', ['$http',function($http){
			
       return {
           get: function(callback)
				{
                $http.get(
                    'menu.json'
                ).
                success(function(data, status) {
					callback(data);
                })
           }
       }
    }]);
	
myApp.factory('XML_Detail', function($http){
    return {
        get: function(code, callback2)
				{
                $http.get(
                    './res/json/'+code+'.json'
                ).
                success(function(data, status) {
						callback2 (data);                                                                                                                                              
                                      
                }).error(function()
                    {                       
                    swal('Error', 'No existeix cap neoplastia amb aquest codi', 'error');
                })
                				
           }
    }
});

$(document).ready(function() {
      
	$("table a").attr("href", "javascript:void(0)");
      
    function detectIE() {
        var ua = window.navigator.userAgent;
    
        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }
    
        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }
    
        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
        // IE 12 (aka Edge) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }
    
        // other browser
        return false;
    }  
    
    $(window).load(function() {
        
        if (navigator.appName == 'Microsoft Internet Explorer')
        {
            var version=detectIE();
            
        }
        else
        {            
            $(".container").animate({
                opacity: 1,       
            }, 1000, function() {
                
            });
        }       
    });
    	
});
