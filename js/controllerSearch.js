
/// <reference path="../typings/jquery/jquery.d.ts"/>
/// <reference path="../typings/angularjs/angular.d.ts"/>
var myApp = angular.module('myApp', ['ivh.treeview']);
var textoBusqueda="";

myApp.config(function(ivhTreeviewOptionsProvider) {
 ivhTreeviewOptionsProvider.set({
   defaultSelectedState: false,
   validate: false,
   expandToDepth: 1,
   useCheckboxes: false,
   labelAttribute: '@label',
   twistieExpandedTpl: "<img src='./img/toggle.png' />",
    twistieCollapsedTpl: "<img src='./img/expand.png' />",
    twistieLeafTpl: "<img src='./img/status.png' />"
 });
});


myApp.controller('ctlRoot',

    function ($scope, $timeout, $filter,XML_Detail,XML_DetailExt)
    {                   
        //No ver info añadida
        $('.infoline').hide();
        
        //Test
        if($('#zoom_searchbox').length>0) $('#zoom_searchbox')[0].value=getParam("zoom_query");
        
        //Cambio de item en Index para aplicar acento
        //var items4=$('.zoom_categories li span');
        //items4[2].innerHTML="e&#205;ndex";
        
        var resIndex2=$('.zoom_categories').html();    
        if(resIndex2 !== undefined)
		{		
            //Imagen
            var item1=$('.zoom_categories li')[1].children[1];
            if(item1!=null) item1.innerHTML="&#205;ndex"; 
            
            var item2=$('.zoom_categories li')[2].children[1];
            if(item2!=null) item2.innerHTML="Tabular";    
            
            if($('.zoom_categories li').length>3)
            {
				var item3=$('.zoom_categories li')[3].children[1];
				if(item3!=null) item3.innerHTML="Causes externes";
            }                                            	
		}
        
        //$('.cat_summary li a')[0].innerText="Tabular";
		if($('.cat_summary li a').length>0)
		{
			if($('.cat_summary li a')[$('.cat_summary li a').length-1].innerText=='eÍndex ') $('.cat_summary li a')[$('.cat_summary li a').length-1].innerText="Causes externes";    
		}
       // if($('.cat_summary li a').length>2) $('.cat_summary li a')[2].innerText="Causes externes";      
       
       GoTop();
              
      //FirstWordFirstLetter();
              
        //Cambio [tabular]
        var resTabular=$('.results').html();      
		if(resTabular !== undefined)
		{
			var regExpTabular = new RegExp('\\[Tabular]', "igm");     		
			var htmlTabular=resTabular.replace(regExpTabular, '<img src="./img/tabular16.png" alt="Tabular" title="Tabular" class="tooltip2" />');
			$('.results').html(htmlTabular);  
		}
        
        //Añadir codigo de Tabular
        var tabulars=$('.result_title').has('img[src="./img/tabular16.png"]');                          
        tabulars.each(function( index ) {
            //console.log( index + ": " + $( this ).text() );
            //var image=index.childrens()[1];            
             var href=tabulars[index].children[2];
             var code=href.getAttribute('href');     
			if(code!=null)
			{
				var codeArray=code.split('.'); 
				var code2=codeArray[codeArray.length-2];                       
				$( this ).append("(<b>"+code2+"</b>)");
			}	             
        });       
        
        var reseIndex=$('.results').html();   

        if(reseIndex !== undefined)
		{
			var regExpeeIndex = new RegExp('\\[e.ndex]', "igm");     
			var htmlIndex=reseIndex.replace(regExpeeIndex, '<img src="./img/eindex16.png" alt="e&#205;ndex" title="Causes externes" class="tooltip2" />');
            $('.results').html(htmlIndex);  
        }  
        
        //Cambio [index] e [eIndex]                        
        var resIndex=$('.results').html();    
		if(resIndex !== undefined)
		{		           
            //var regExpeeIndex = new RegExp('\\[e.ndex]', "igm");     
			//var htmlIndex=resIndex.replace(regExpeeIndex, '<img src="./img/eindex16.png" alt="e&#205;ndex" title="e&#205;ndex" class="tooltip2" />');
            
			var regExpIndex = new RegExp('\\[.ndex]', "igm");     
			htmlIndex=resIndex.replace(regExpIndex, '<img src="./img/index16.png" alt="&#205;ndex" title="&#205;ndex" class="tooltip2" />');                       
            
			$('.results').html(htmlIndex);  
		}       
                      
        
        /*
        $( "#zoom_searchbox" ).change(function() {
            textoBusqueda=$('#zoom_searchbox').val();
            $('#txtTextoBusqueda')[0].value=textoBusqueda;            
        });
        */
        
        
        //Cambio [eindex]                        
       /* var reseIndex=$('.results').html();    
		if(resIndex !== undefined)
		{		
			var regExpeIndex = new RegExp('\\[E.ndex]', "igm");     
			var htmleIndex=reseIndex.replace(regExpeIndex, '<img src="./img/index16.png" alt="e&#205;ndex" title="e&#205;ndex" class="tooltip2" />');
			$('.results').html(htmleIndex);  
		}        
        
        $( "#zoom_searchbox" ).change(function() {
            textoBusqueda=$('#zoom_searchbox').val();
            $('#txtTextoBusqueda')[0].value=textoBusqueda;            
        });
        */
               
        
        //Evento click de los (códigos) en la parte derecha. Van a buscar al Tabular
        //$('.click2').on('click',function(event)
        $(document).bind('click', ".click2", function(event)
        //$scope.codeClick = function (event) 
        {	 
            var code=event.target.text;
            var codeOK="";
            
            if(code=='')return;
            
            //Es nula
            if(typeof code === 'undefined') return;
            
            //Es "Seguent >>"
            if (code.match("ent >>$")) return;
            
            if (event.target.className.indexOf("click2") != -1)
            {
                codeOK=code;
                var pointcode=code.indexOf('.');
                $('#txtRedirect')[0].value=code;
                while($('#txtRedirect')[0].value.indexOf('.')!=-1) $('#txtRedirect')[0].value=$('#txtRedirect')[0].value.replace('.','-');
                if(pointcode>-1)
                {
                    codeOK=code.substring(0,pointcode);
                }
            }  
            else
            {                              
                var pointcode=code.indexOf('.');
                if(pointcode==-1) return;
                $('#txtRedirect')[0].value=code;
                while($('#txtRedirect')[0].value.indexOf('.')!=-1) $('#txtRedirect')[0].value=$('#txtRedirect')[0].value.replace('.','-');
                codeOK=code.substring(0,pointcode);
            }    
            
            if(codeOK=='') return;      
                        
            
            $(".divDetail2").hide();
            //$(".divDetailExt2").hide();
            $(".divDetailExt").hide();
            $(".divDetail").hide();  
            $(".divDetail2").fadeTo( "fast" , 0);
            //$(".divDetailExt2").fadeTo( "fast" , 0);
            $(".divDetailExt").fadeTo( "fast" , 0);
            $(".divDetail").fadeTo( "fast" , 0);
            
            XML_Detail.get(codeOK,function(rest)
			{                   
			       //Cuando llega el dato... 
					$scope.dbTabular=rest;                         
                                                                          
                    //Cambio subrayar amarillo palabras de búsqueda                        
                    window.setTimeout(function()
                    {
                     var val=getParam("zoom_query"); 
                     var html2=$('.divDetail').html();                                                                                                                             
                     var words = val.split(" ");                                                                    
                     $(".divDetail").hide();           
                     $('.divDetail2').html(html2);
                     MarcarAmarillo();                          
                     $(".divDetail2").fadeTo( "fast" , 1 );  
                     $('html,body').animate(
                        {                                                     
                         scrollTop: $('.divDetail2').offset().top-180},'slow');
                        MakeTree();
                    }, 500);  
                               
            });
            event.preventDefault();
        });            
        
        //Evento click de la izquierda hacia la derecha
        $('.result_title a').on('click', function(event){ 
            $(".divDetail2").fadeTo( "fast" , 0 );        
              var echo="";
              if(event.target.className=='highlight')
              {
                  echo=event.target.parentElement.pathname;
              }
              else
              {
                  echo=event.target.pathname;
              }              			    		                    
            
            var pos=0;
            var indexPos=echo.indexOf('.index.');
            var tabuPos=echo.indexOf('.tabular.');
            var eindexPos=echo.indexOf('.e_index.');
            
            //Si no tiene especificado un código de Tabular, cargo un json global de la carpeta /RES/EXT
            if(indexPos>0 || eindexPos>0)
            { 
                event.preventDefault();
                $(".divDetail2").fadeTo( "fast" , 0);
                $(".divDetail2").hide();             
                
                var last1= echo.lastIndexOf("/");
                
                //Quito index, tabular                
                echo=echo.replace('._.html','._.json');                
                var extCode=echo.substr(last1+1);
                                
                
                XML_DetailExt.get(extCode,function(rest)
				 {
                   //LLega el dato y pongo en pantalla                                                                                               
					$scope.dbTabularExt=rest;      
                                        
                    window.setTimeout(function(){
                     var val=getParam("zoom_query"); 
                     var html2=$('.divDetailExt').html();                                                                                                                                
                     var words = val.split(" ");

                     $(".divDetailExt").show();
                     MarcarAmarillo();
                      $(".divDetailExt").fadeTo( "fast" , 1 ); 
                       
                    $('html,body').animate(
                        {                                                     
                         scrollTop: $('.divDetailExt').offset().top-180                        
                        },'slow');
                        
                         
                    }, 500);                                           
                 });                    
                 return;                      
            }   
                                             
            
            if(indexPos>0) pos=indexPos+7;
            if(tabuPos>0) pos=tabuPos+9;
            if(eindexPos>0) pos=eindexPos+9;
            
            if(pos==0)
            { 
                event.preventDefault();
                $(".divDetail2").fadeTo( "fast" , 1);
                return;                
                
            }             
                
            var code=echo.substring(pos,(pos+3));
             
            
            XML_Detail.get(code,function(rest)
				 {
                   $(".divDetailExt").hide();
                   
					$scope.dbTabular=rest;                        
                                                                          
                    //Cambio subrayar amarillo palabras de búsqueda                        
                    window.setTimeout(function(){
                     var val=GetAccents($('#zoom_searchbox').val()); 
                     var html2=$('.divDetail').html();                                                                                                                                
                     var words = val.split(" ");
                     
                     if(words.length==2)
                     {
                         var one=words[0].length;
                         var two=words[1].length;
                         if(two<2)
                         {
                             words=words[0]+" "+words[1].toUpperCase();
                         }
                     }

                     $('.divDetail2').html(html2); 
                     MarcarAmarillo();
                       $(".divDetail2").fadeTo( "fast" , 1 );  
                    $('html,body').animate(
                        {                                                     
                         scrollTop: $('.titulo').offset().top-($('.titulo2').outerHeight()+50)
                        },'slow');
                      MakeTree();
                    }, 500);  
                                                                              
				 });
            
			event.preventDefault();
    		// or alert($(this).hash();
  		});
          
          function UpperFirstLetter(string) {
                return string.charAt(0).toUpperCase() + string.slice(1);
            }
            
            function LowerFirstLetter(string) {
                return string.charAt(0).toLowerCase() + string.slice(1);
            }
            
            function FirstWordFirstLetter(){
                //Mayuscula la primera letra de la primera palabra
       var mayus=$(".upperCaseFirstChar");       
       mayus.each(function() {
           var ma=$(this).html();
           var ma2=capitalizeSentences(ma);           
           $(this).html(ma2);
       });

            }
            
            
            function GetAccents(string)
            {
                var w=string;                
                while(w.indexOf('*')!=-1) w=w.replace('*','');
                while(w.indexOf('?')!=-1) w=w.replace('?','');                             
							
                return w;
            }
            
            function GoTop()
        {
             $(window).scroll(function(){
            if ($(this).scrollTop() > 50) {
                $('#backToTop').fadeIn('slow');
            } else {
                $('#backToTop').fadeOut('slow');
            }
        });
        
            $('#backToTop').click(function(){
                $("html, body").animate({ scrollTop: 0 }, 500);
                return false;
            });
        }
        
        function capitalizeSentences(string)
        {
            var capText =string;
            var allCaps = false;
            var capLock = false;
            
            
            if(capLock == 1 || capLock == true){
            capText = capText.toLowerCase();
            }
            
            
            if(allCaps == 1 || allCaps == true){
            
            capText = capText.replace(/\n/g,". [-<br>-] ");
            var wordSplit = ' ';
            
            }else{
            capText = capText.replace(/\.\n/g,".[-<br>-]. ");
            capText = capText.replace(/\.\s\n/g,". [-<br>-]. ");
            var wordSplit = '. ';
            }

            var wordArray = capText.split(wordSplit);
            
            var numWords = wordArray.length;
            
            for(x=0;x<numWords;x++) {
            
                wordArray[x] = wordArray[x].replace(wordArray[x].charAt(0),wordArray[x].charAt(0).toUpperCase());
                
                if(allCaps == 1 || allCaps == true){
                        if(x==0) {
                            capText = wordArray[x]+" ";
                        }else if(x != numWords -1){
                            capText = capText+wordArray[x]+" ";
                        }else if(x == numWords -1){
                            capText = capText+wordArray[x];
                        }
                }else{
                        if(x==0) {
                            capText = wordArray[x]+". ";
                        }else if(x != numWords -1){
                            capText = capText+wordArray[x]+". ";
                        }else if(x == numWords -1){
                            capText = capText+wordArray[x];
                        }
                
                }
                
                
            }
            
            if(allCaps == 1 || allCaps == true){
            
            capText = capText.replace(/\.\s\[-<br>-\]\s/g,"\n");
            capText = capText.replace(/\.\s\[-<br>-\]/g,"\n");
            
            
            }else{
            capText = capText.replace(/\[-<br>-\]\.\s/g,"\n");
            }
            
            capText = capText.replace(/\si\s/g," I ");      	
            return capText;

        }           
            
        function capitalize(string) {
                var i, words, w, result = '';
                
                words = string.split(' ');
            
                for (i = 0; i < words.length; i += 1) {
                    w = words[i];
                    result += w.substr(0,1).toUpperCase() + w.substr(1);
                    if (i < words.length - 1) {
                        result += ' ';    // Add the spaces back in after splitting
                    }
                }
            
                return result;
         }
        
       
        $scope.showActivity = function (msg) {
            $timeout(function () {
                $scope.activityMessage = msg;
            });
        };
        $scope.hideActivity = function () {
            $timeout(function () {
                $scope.activityMessage = '';
            }, 1000); // message will be visible at least 1 sec
        };
		
		$scope.showActivityDetail = function (msg) {
            $timeout(function () {
                $scope.activityMessageDetail = msg;
            });
        };
        $scope.hideActivityDetail = function () {
            $timeout(function () {
                $scope.activityMessageDetail = '';
            }, 1000); // message will be visible at least 1 sec
        };
		$scope.withoutDash = function (string) {
            while(string.indexOf('.')!=-1) string=string.replace('.','-');
            return string;
        };
		$scope.nodeClick = function (nodo) {			 
			 if (!nodo.hasOwnProperty('children'))
			 {
				 var code=nodo['@label'];
				 var codeClear=code.substring(0, 4);
				 codeClear=$.trim(codeClear);
				 
				 XML_Detail.get(codeClear,function(rest)
				 {
                    $scope.dbTabular=null;
                    /*
                    var val=$('#zoom_searchbox').val();  
					val=GetAccents(val);
                   
                    var rep=rest;  
                    rep=rep.replace(val,'<span class="highlight">'+val+'</span>');                
					
					
                    $scope.dbTabular=rep;		
                    */
                    $scope.dbTabular=rest;		
                    MarcarAmarillo();
					$('html,body').animate({ scrollTop: 0 }, 'slow', function () {						
						
						
                    });
						
				 });
			 }
		};              
		
        // So here is how we do it:
        // "Before" the process, we set the message and the activity indicator is shown
        $scope.showActivity('Loading items...');

        // "After" the process completes, we hide the activity indicator.
        $scope.hideActivity();

        //Ctrl Izquierda
        //This is the callback function
        $scope.showActivity('Loading...');
        setData = function(data)
        {
			$scope.showActivityDetail("Loading detail...");
            $scope.dbMain = data;
            $('#txtFilter').focus();
			$scope.hideActivityDetail();
        }

        $scope.buttonClickHandler = function()
        {
            $scope.showActivity('Filtering items...');
            $scope.myFilter = $scope.myFilterTemp;
            $scope.hideActivity();
        }

        $scope.GetDetail = function($event, name)
        {
            $scope.myFilterDetail=name.title;
        }
       
	   $scope.expand = function(element,nodo)
	   {
		   element  = angular.element(element);
		   var newDirective = angular.element("<ul ng-repeat='lv2 in lv1.C'><li class='liItem' ng-click='GetDetail($event, lv2)'><img src='./img/bullet2.png' /><b>{{lv2._I}}</b> - {{lv2._D}}</li></ul>");
		   element.append(newDirective);
		    $compile(newDirective)($scope);
		   
	   }	 
       
       $scope.GetArrayNotes=function(str) {  
			var v=str;		
            var res2 = new Array();
                         
            var res = v.match(/[A-Z]{1}[0-9]{2}/gi);
                               
            if(res.length==0) return res2;                           
                               
            v=v.replace(res[0],'');                          
            res2.push(v);
                        
            return res2;           						
        }  	 
        
        $scope.GetArrayNotes2=function(str) {  
			var v=str;		
                         
            var res = v.match(/[A-Z]{1}[0-9]{2}/gi);                                           
                
            return res;           						
        }  	    		
         
        //Miro los items que hay como resultados y si no hay seleccionado ninguno, selecciono el primero
        var countResults=$(".result_title").length;
        if(countResults>0)
        {
            var first=$(".result_title").first();
            first.children("a")[0].click();
        }       
                
         //Tooltip        
        $('.tooltip2').tooltipster({
            theme: 'tooltipster-shadow'
        });              
			                       	   
	   $('#txtFind').keyup(function() {
		    if($('#txtFind')[0].value=='')
			{
		   setTimeout(function () {
			$scope.$apply(function () {
				DesactivarDetalle();				 
			});
		}, 500);
		}
		});	  
		
		function DesactivarDetalle()
		{
			$( "#divDetalle" ).fadeTo( "slow" , 0, function() {
				//$scope.dbTabular=null;
			});
		}
		function ActivarDetalle()
		{
			$( "#divDetalle" ).fadeTo( "slow" , 1, function() {
			// Animation complete.
			});
		}
        
        function TreeViewImage()
        {
            $('li:has(ul):not(.except, .active) img').each(function() {                
                if($(this).parent().parent().parent().parent().parent().parent().children('ul').length>0)
                {
                    $(this).attr("src", "./img/expand.png");
                }
                else
                {
                    
                }
            });
        }
        
        function MarcarAmarillo()
          {
              var val=GetAccents($('#zoom_searchbox').val()); 
              var words = val.split(" ");
              
              $(".divDetail2").highlight(val, { wordsOnly: true });
              $(".divDetailExt2").highlight(val,  { wordsOnly: true });
              
              for (var i = 0; i < words.length; i++) 
              {                  
                  $(".divDetail2").highlight(words[i], { wordsOnly: true });
                  $(".divDetailExt2").highlight(words[i],  { wordsOnly: true });
              }
          }
        
        function MakeTree()
        {
            $( '.tree li' ).each( function() {
						if( $( this ).children( 'ul' ).length > 0 ) {
								$( this ).addClass( 'parent' );     
						}
				});
				
				$( '.tree li.parent a' ).click( function( ) {
						$( this ).parent().parent().parent().parent().parent().toggleClass( 'active' );
						$( this ).parent().parent().parent().parent().parent().children( 'ul' ).slideToggle( 'fast' );
                        if($(this).parent('td').parent().parent().parent().parent().children('ul').length>0)
                        {
                            if($( this ).children().attr("src")=="./img/expand.png")
                            {
                                $( this ).children().attr("src", "./img/toggle.png");
                            }
                            else
                            {
                                $( this ).children().attr("src", "./img/expand.png");
                            }
                        }
                        
                        $('.clicked').removeClass('clicked');
				    //$( this ).parent().parent().parent().parent().toggleClass('clicked');
                        $(this).parent().parent().parent().toggleClass('clicked');
				});								
				
				$( '.tree li' ).each( function() {
						$( this ).toggleClass( 'active' );
						$( this ).children( 'ul' ).slideToggle( 'fast' );
				});
                
                $( '#all' ).click( function() {					
					$( '.tree li.parent a' ).not('.except').each( function() {
                        $( this ).parent().parent().parent().parent().parent().toggleClass( 'active' );		
                        $( this ).parent().parent().parent().parent().parent().children( 'ul' ).slideToggle( 'fast' );                                                									
					});
				});
                
               // $( '#all' ).click( function() {					
					$( '.tree li.parent a' ).not('.except').each( function() {
                        $( this ).parent().parent().parent().parent().parent().toggleClass( 'active' );		
                        $( this ).parent().parent().parent().parent().parent().children( 'ul' ).hide();										
					});
                   
                    $( '.except' ).not('.except').each( function() {
                        $( this ).parent().parent().parent().parent().parent().addClass( 'active' );		
                        $( this ).parent().parent().parent().parent().parent().children( 'ul' ).show();										
					});
				//});
                
		      //Si hay algo seleccionado, abro el li
              var selected=$('#txtRedirect')[0].value;
              if(selected)
              {                  
                  $('#'+selected).children( 'ul' ).slideToggle( 'fast' );
                  $('#'+selected).addClass( 'active' );	
                  $('#'+selected).addClass( 'clicked' );
               
                  //Si tiene decimales
                  if(selected.indexOf('-')>0)
                  {
                      var posDash=selected.indexOf('-');
                      var long=selected.length;
                      var dif=long-posDash;
                      switch(dif)
                      {
                          //Tres decimales
                          case 4:                          
                          //Nivel 2
                          // $('#'+selected).parent().parent().children('ul').slideToggle( 'fast' );                                                      
                           //$('#'+selected).addClass( 'active' );
                           $('#'+selected).parent('ul').slideToggle( 'fast' );
                           $('#'+selected).parent('ul').addClass( 'active' );
                           
                           $('#'+selected).parent('ul').parent('li').parent('ul').slideToggle( 'fast' );
                           $('#'+selected).parent('ul').parent('li').parent('ul').addClass( 'active' );
                            break;  
                          //Dos decimales
                          case 3:
                            //$('#'+selected).parent().parent().children( 'ul' ).slideToggle( 'fast' );
                            //$('#'+selected).parent().parent().addClass( 'active' );
                            $('#'+selected).parent('ul').slideToggle( 'fast' );
                           $('#'+selected).parent('ul').addClass( 'active' );
                           
                           //$('#'+selected).parent('ul').parent('li').parent('ul').slideToggle( 'fast' );
                           //$('#'+selected).parent('ul').parent('li').parent('ul').addClass( 'active' );
                            break;                            
                            //Un decimal
                          case 2:
                            //$('#'+selected).children( 'ul' ).slideToggle( 'fast' );
                            $('#'+selected).addClass( 'active' );
                            //$('#'+selected).addClass('clicked');
                            break;
                      }                      
                  }
              }                    
              TreeViewImage();     
        }
        
        $scope.rootArray = [];
        $scope.GetArrayLevel=function(obj) {
            return jQuery.makeArray(obj)
        } 
        
        $scope.GetArrayByComma=function(str) {  
            if(str == null)
            { 
                return '';
            }
                   
            var ar=$.makeArray(str.split(',')); 

            return ar;
        } 

		$scope.GetArrayByDash=function(str) {              
            if(str == null) return '';
                   
            var ar=$.makeArray(str.split('-')); 
            return ar;
            
        }
        
        $scope.GetUrlFind=function(str) {
            var url=$(location).attr('href');
            var start=url.indexOf("zoom_query=");
            var end=url.indexOf("&");   
            
            start+=11;
            var url2=url.substring(0,start);
            url2+=str;   
            var endStr=url.substring(end,end+(url.length-end));         
            url2+=endStr;    
            return url2
        }  
        
		$scope.GetTrim=function(str) {            
            return $.trim(str);
        }	
        	
		$scope.GetClear=function(str) {  
			var v=str;		
            while(v.indexOf('.-)')!=-1) v=v.replace('.-)',')');
			while(v.indexOf('-)')!=-1) v=v.replace('-)',')');
			while(v.indexOf('-, ')!=-1) v=v.replace('-, ',', ');	
			
            var regExpIndex = new RegExp('[A-Za-z]{1}[0-9]{2}', "igm");     
			v=v.replace(regExpIndex, "<a href='#' class='click2 tooltip3'>Codi</a>");			  
			
			return v;
        }
        
        $scope.GetClearNombre=function(str) {  
			var v=str;		
            while(v.indexOf('.-)')!=-1) v=v.replace('.-)',')');
			while(v.indexOf('-)')!=-1) v=v.replace('-)',')');
			while(v.indexOf('-, ')!=-1) v=v.replace('-, ',', ');		
			
            return v.split('(')[0];
            var regExpIndex = new RegExp('[A-Za-z]{1}[0-9]{2}', "igm");     
			v=v.replace(regExpIndex, "<a href='#' class='click2 tooltip3'>Codi</a>");			  
			
			return v;
        }
        
        $scope.GetClearNombre2=function(str) {  
			var resSTR = str.match(/\(([A-Z])\d\d.?\d{0,3}-?/g);
            if(resSTR==null) return str;
            if(resSTR.indexOf(',')==-1)
            {
                str=str.substring(0,str.indexOf(resSTR));
                return str;
            }
            
            str=str.substring(0,resSTR.indexOf(',')[0]);
            return str;            
        }
        
        $scope.GetArrayCodigos=function(str) {  
			 var res = new Array();            
            var resSTR = str.match(/([A-Z])\d\d.?\d{0,3}-?/g);
            if(resSTR==null) return res;                       
            
            switch(resSTR.length){
                case 0:
                    return '';
                    break;
                case 1:                                        
                    var val2=resSTR[0];
                    val2=val2.replace('-','');
                    val2=val2.replace(',','');
                    val2=val2.replace(')','');
                    val2=val2.replace('(','');
                    val2=$.trim(val2);
                    res.push(val2);
                                       
                    return res;
                    break;
                default:
                    var i=0;
                    for (i = 0; i < resSTR.length; ++i) 
                    {
                            var val=resSTR[i];
                            val=val.replace('-','');
                            val=val.replace(',','');
                            val=val.replace(')','');
                            val=val.replace('(','');      
                            val=$.trim(val); 
                            res.push(val);
                    }                    
                    break;
            }                                   
            return res;         						
        }       
        
        $scope.GetClearCodigo=function(str) {  
			var v=str;		
            while(v.indexOf('.-)')!=-1) v=v.replace('.-)',')');
			while(v.indexOf('-)')!=-1) v=v.replace('-)',')');
			while(v.indexOf('-, ')!=-1) v=v.replace('-, ',', ');		
			
            return v.split('(')[1].replace(")","");
            var regExpIndex = new RegExp('[A-Za-z]{1}[0-9]{2}', "igm");     
			v=v.replace(regExpIndex, "<a href='#' class='click2 tooltip3'>Codi</a>");			  
			
			return v;
        }
        
        $scope.IsGetClear=function(str) {  
			var v=str;		
            while(v.indexOf('.-)')!=-1) v=v.replace('.-)',')');
			while(v.indexOf('-)')!=-1) v=v.replace('-)',')');
			while(v.indexOf('-, ')!=-1) v=v.replace('-, ',', ');	
			
            var vOld = v;
            var regExpIndex = new RegExp('[A-Za-z]{1}[0-9]{2}', "igm");     
			v=v.replace(regExpIndex, "<a href='#' class='click2 tooltip3'>Codi</a>");			  
			
			return v != vOld;
        }
        
		$scope.GetClear2=function(str) {  
			var v=str;		          
			v=v.replace('-','');			
						
			return v;
        }

    });
	
	
myApp.factory('XML_Detail', function($http){
    return {
        get: function(code, callback2)
				{
                $http.get(
                    './res/json/'+code+'.json'
                ).
                success(function(data, status) {
					
						callback2 (data);
				
                })				
           }
    }
});

myApp.factory('XML_DetailExt', function($http){
    return {
        get: function(code, callback2)
				{
                $http.get(
                    './res/ext/'+code
                ).
                success(function(data, status) {
					
						callback2 (data);
				
                })				
           }
    }
});
