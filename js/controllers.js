var myApp = angular.module('myApp', ['myApp.service','myApp.service2','cgBusy']);

myApp.controller('ctlRoot',

    function ($scope, $timeout, $filter,DataSource,DataSource2)
    {
        // You would better place these two methods below, inside a
        // service or factory; so you can inject that service anywhere
        // within the app and toggle the activity indicator on/off where needed
        $scope.showActivity = function (msg) {
            $timeout(function () {
                $scope.activityMessage = msg;
            });
        };
        $scope.hideActivity = function () {
            $timeout(function () {
                $scope.activityMessage = '';
            }, 1000); // message will be visible at least 1 sec
        };
		
		$scope.showActivityDetail = function (msg) {
            $timeout(function () {
                $scope.activityMessageDetail = msg;
            });
        };
        $scope.hideActivityDetail = function () {
            $timeout(function () {
                $scope.activityMessageDetail = '';
            }, 1000); // message will be visible at least 1 sec
        };
		
        // So here is how we do it:
        // "Before" the process, we set the message and the activity indicator is shown
        $scope.showActivity('Loading items...');

        // "After" the process completes, we hide the activity indicator.
        $scope.hideActivity();




        //Ctrl Izquierda
        //This is the callback function
        $scope.showActivity('Loading...');
        setData = function(data)
        {
			$scope.showActivityDetail("Loading detail...");
            $scope.dbMain = data;
            $('#txtFilter').focus();
			$scope.hideActivityDetail();
            //$scope.dbMain=data;
            //$scope.dbMain=data;
        }

        DataSource.get(setData);

        $scope.buttonClickHandler = function()
        {
            $scope.showActivity('Filtering items...');
            $scope.myFilter = $scope.myFilterTemp;
            $scope.hideActivity();
        }

        $scope.GetDetail = function($event, name)
        {
            //alert("Called " + name.title);
            $scope.myFilterDetail=name.title;
        }

        //CtrlDerecha
        setData2 = function(data2)
        {
            $scope.dbTabular = data2;
        }

        DataSource2.get(setData2);

    });

angular.module('myApp.service',[]).
    factory('DataSource', ['$http',function($http){
			
       return {
           get: function(callback)
				{
                $http.get(
                    './res/Index.xml',
                    {transformResponse:function(data) {
                    	// convert the data to JSON and provide
                    	// it to the success function below
						var x2js = new X2JS();
						var json = x2js.xml_str2json( data );
						return json;
						}
					}
                ).
                success(function(data, status) {
					// send the converted data back
					// to the callback function	
                    callback(data);
                })
           }
       }
    }]);
	
angular.module('myApp.service2',[]).
	factory('DataSource2', ['$http',function($http){
			
       return {
           get: function(callback2)
				{
					$http.get(
                    './res/Tabular.xml',
                    {transformResponse:function(data2) {
                    	// convert the data to JSON and provide
                    	// it to the success function below
						var x2js = new X2JS();
						var json = x2js.xml_str2json(data2);
						return json;
						}
					}
                ).
                success(function(data2, status) 
				{
					// send the converted data back
					// to the callback function	
                    callback2(data2);
                })
           }
       }
    }]);
    
//angular.module('myApp',['myApp.service']);
//angular.module('myApp',['myApp.service2']);

/*var AppController = function($scope,DataSource) {
    
    //This is the callback function
    setData = function(data) {
        $scope.dbMain = data;
    }
        
    DataSource.get(setData);
    
}*/






