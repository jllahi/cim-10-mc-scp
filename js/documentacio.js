var DEMO = (function ($) {
  'use strict';

  var $grid = $('#grid'),
    $filterOptions = $('.filter-options'),
    $sizer = $grid.find('.shuffle__sizer'),

    init = function () {


      // None of these need to be executed synchronously
      setTimeout(function () {
        listen();
        setupFilters();
        setupSorting();
        setupSearching();
      }, 100);

      // You can subscribe to custom events.
      // shrink, shrunk, filter, filtered, sorted, load, done
      $grid.on('loading.shuffle done.shuffle shrink.shuffle shrunk.shuffle filter.shuffle filtered.shuffle sorted.shuffle layout.shuffle', function (evt, shuffle) {
        // Make sure the browser has a console
        if (window.console && window.console.log && typeof window.console.log === 'function') {
          //console.log( 'Shuffle:', evt.type );
        }
      });

      // instantiate the plugin
      $grid.shuffle({
        itemSelector: '.picture-item',
        sizer: $sizer
      });

      // Destroy it! o_O
      // $grid.shuffle('destroy');
    },

    // Set up button clicks
    setupFilters = function () {
      var $btns = $filterOptions.children();
      $btns.on('click', function () {
        var $this = $(this),
          isActive = $this.hasClass('active'),
          group = isActive ? 'all' : $this.data('group');

        // Hide current label, show current label in title
        if (!isActive) {
          $('.filter-options .active').removeClass('active');
        }

        $this.toggleClass('active');

        // Filter elements
        $grid.shuffle('shuffle', group);

        //Quitamos divs
        switch (group) {
          case "MC":
            $('#divMC').show(200);
            $('#divMPC').hide(200);
            break;
          case "MPC":
            $('#divMC').hide(200);
            $('#divMPC').show(200);
            break;
          case "MA":
            $('#divMC').hide(200);
            $('#divMPC').hide(200);
            break;
        }
      });

      $btns = null;
      $grid.shuffle('shuffle', "MC");
      $('#divMPC').hide();

    },

    setupSorting = function () {
      // Sorting options
      $('.sort-options').on('change', function () {
        var sort = this.value,
          opts = {};

        // We're given the element wrapped in jQuery
        if (sort === 'date-created') {
          opts = {
            reverse: true,
            by: function ($el) {
              return $el.data('date-created');
            }
          };
        } else if (sort === 'title') {
          opts = {
            by: function ($el) {
              return $el.data('title').toLowerCase();
            }
          };
        }

        // Filter elements
        $grid.shuffle('sort', opts);
      });
    },

    setupSearching = function () {
      // Advanced filtering
      $('.js-shuffle-search').on('keyup change', function () {
        var val = this.value.toLowerCase();
        $grid.shuffle('shuffle', function ($el, shuffle) {

          // Only search elements in the current group
          if (shuffle.group !== 'all' && $.inArray(shuffle.group, $el.data('groups')) === -1) {
            return false;
          }

          var text = $.trim($el.find('.picture-item__title').text()).toLowerCase();
          return text.indexOf(val) !== -1;
        });
      });
    },

    // Re layout shuffle when images load. This is only needed
    // below 768 pixels because the .picture-item height is auto and therefore
    // the height of the picture-item is dependent on the image
    // I recommend using imagesloaded to determine when an image is loaded
    // but that doesn't support IE7
    listen = function () {
      // var debouncedLayout = $.throttle( 300, function() {
      //   $grid.shuffle('update');
      // }
      //	);

      // Get all images inside shuffle
      $grid.find('img').each(function () {
        var proxyImage;

        // Image already loaded
        if (this.complete && this.naturalWidth !== undefined) {
          return;
        }

        // If none of the checks above matched, simulate loading on detached element.
        proxyImage = new Image();
        $(proxyImage).on('load', function () {
          $(this).off('load');
          //debouncedLayout();
        });

        proxyImage.src = this.src;
      });

      // Because this method doesn't seem to be perfect.
      setTimeout(function () {
        //debouncedLayout();
      }, 500);
    };

  return {
    init: init
  };
}(jQuery));


$(document).ready(function () {

  $(".imgMC_Index").bind("click", function () {
    $.colorbox({ iframe: true, title: '&Iacute;ndex de malalties i lesions', href: "./pdf/MC/index.pdf", innerWidth: 960, innerHeight: 500 });
  });
  $(".imgMC_e-Index").bind("click", function () {
    $.colorbox({ iframe: true, title: '&Iacute;ndex de causes externes de lesions', href: "./pdf/MC/eIndex.pdf", innerWidth: 960, innerHeight: 500 });
  });
  $(".imgMC_Neo").bind("click", function () {
    $.colorbox({ iframe: true, title: 'Taula de neopl&agrave;sies', href: "./pdf/MC/neo.pdf", innerWidth: 960, innerHeight: 500 });
  });
  $(".imgMC_Tabular").bind("click", function () {
    $.colorbox({ iframe: true, title: 'Llista tabular de malalties i lesions', href: "./pdf/MC/tabular.pdf", innerWidth: 960, innerHeight: 500 });
  });
  // $(".imgMC_Guies").bind("click", function () {
  //   $.colorbox({ iframe: true, title: "Directrius per a la codificaci&oacute; i l'elaboraci&oacute; d'informes de malalties", href: "./pdf/MC/icd10cm-guidelines-2015.pdf", innerWidth: 960, innerHeight: 500 });
  // });
  // $(".imgMC_Codis").bind("click", function () {
  //   $.colorbox({ iframe: true, title: 'Codis duplicats', href: "./pdf/MC/duplicate_ICD10CM_codes.pdf", innerWidth: 960, innerHeight: 500 });
  // });
  $(".imgMC_Medicines").bind("click", function () {
    $.colorbox({ iframe: true, title: 'Taula de f&agrave;rmacs i subst&agrave;ncies qu&iacute;miques', href: "./pdf/MC/drugs.pdf", innerWidth: 960, innerHeight: 500 });
  });

  $(".imgMPC_Procediments").bind("click", function () {
    $.colorbox({ iframe: true, title: 'Taules i &iacute;ndex de procediments', href: "./pdf/PCS/IndexPCS.pdf", innerWidth: 960, innerHeight: 500 });
  });
  // $(".imgMPC_Arxius").bind("click", function () {
  //   $.colorbox({ iframe: true, title: 'Arxius de registre de la CIM-10-MC/SPC', href: "./pdf/PCS/ICD10OrderFiles.pdf", innerWidth: 960, innerHeight: 500 });
  // });
  // $(".imgMPC_Directrius").bind("click", function () {
  //   $.colorbox({ iframe: true, title: "Directrius per a la codificaci&oacute; i l'elaboraci&oacute; d'informes de procediments", href: "./pdf/PCS/2015-PCS-guidelines.pdf", innerWidth: 960, innerHeight: 500 });
  // });

  $(".imgMPC_MCTabular").bind("click", function () {
    $.colorbox({ iframe: true, title: "Classificaci&oacute; internacional de malalties, 10a revisi&oacute;, Modificaci&oacute; cl&iacute;nica. Tabular", href: "./pdf/MA/Manual_CIM-10-MC_Tabular.pdf", innerWidth: 960, innerHeight: 500 });
  });
  $(".imgMPC_PCSTabulare").bind("click", function () {
    $.colorbox({ iframe: true, title: 'Classificaci&oacute; internacional de malalties, 10a revisi&oacute;, Sistema de codificaci&oacute; de procediments', href: "./pdf/MA/Manual_CIM-10-SCP_Tabular.pdf", innerWidth: 960, innerHeight: 500 });
  });
  $(".imgMPC_MCCercalliure").bind("click", function () {
    $.colorbox({ iframe: true, title: 'Classificaci&oacute; internacional de malalties, 10a revisi&oacute;, Modificaci&oacute; cl&iacute;nica. Cerca lliure', href: "./pdf/MA/Manual_CIM-10-MC_Cerca_lliure.pdf", innerWidth: 960, innerHeight: 500 });
  });
  $(".imgMPC_MCNeo").bind("click", function () {
    $.colorbox({ iframe: true, title: 'Classificaci&oacute; internacional de malalties, 10a revisi&oacute;, Modificaci&oacute; cl&iacute;nica. Neopl&agrave;sies', href: "./pdf/MA/Manual_CIM-10-MC_Neoplasies.pdf", innerWidth: 960, innerHeight: 500 });
  });
  $(".imgMPC_MCFar").bind("click", function () {
    $.colorbox({ iframe: true, title: 'Classificaci&oacute; internacional de malalties, 10a revisi&oacute;, Modificaci&oacute; cl&iacute;nica. F&agrave;rmacs', href: "./pdf/MA/Manual_CIM-10-MC_Farmacs.pdf", innerWidth: 960, innerHeight: 500 });
  });

  //Tooltip        
  $('.tooltip2').tooltipster({
    theme: 'tooltipster-shadow',
    touchDevices: true,
    multiple: true
  });

  $('.imgMC_Index').tooltipster({
    animation: 'swing',
    content: $('<span><img src="./img/pdf_small.png" style="margin-right:5px;" />&Iacute;ndex de malalties i lesions</span>'),
    multiple: true,
    position: 'left'
  });

  $('.imgMC_e-Index').tooltipster({
    animation: 'swing',
    content: $('<span><img src="./img/pdf_small.png" style="margin-right:5px;" />&Iacute;ndex de causes externes de lesions</span>'),
    multiple: true,
    position: 'left'
  });

  $('.imgMC_Neo').tooltipster({
    animation: 'swing',
    content: $('<span><img src="./img/pdf_small.png" style="margin-right:5px;" />Taula de neopl&agrave;sies</span>'),
    multiple: true,
    position: 'left'
  });

  $('.imgMC_Tabular').tooltipster({
    animation: 'swing',
    content: $('<span><img src="./img/pdf_small.png" style="margin-right:5px;" />Llista tabular de malalties i lesions</span>'),
    multiple: true,
    position: 'left'
  });

  $('.imgMC_Guies').tooltipster({
    animation: 'swing',
    content: $("<span><img src='./img/pdf_small.png' style='margin-right:5px;' />Directrius per a la codificaci&oacute; i l'elaboraci&oacute; d'informes de malalties</span>"),
    multiple: true,
    position: 'left'
  });

  $('.imgMC_Codis').tooltipster({
    animation: 'swing',
    content: $('<span><img src="./img/pdf_small.png" style="margin-right:5px;" />Codis duplicats</span>'),
    multiple: true,
    position: 'left'
  });

  $('.imgMC_Medicines').tooltipster({
    animation: 'swing',
    content: $("<span><img src='./img/pdf_small.png' style='margin-right:5px;' />Taula de f&agrave;rmacs i subst&agrave;ncies qu&iacute;miques</span>"),
    multiple: true,
    position: 'left'
  });

  $('.imgMPC_Procediments').tooltipster({
    animation: 'swing',
    content: $('<span><img src="./img/pdf_small.png" style="margin-right:5px;" />Taules i &iacute;ndex de procediments</span>'),
    multiple: true,
    position: 'left'
  });

  $('.imgMPC_Arxius').tooltipster({
    animation: 'swing',
    content: $('<span><img src="./img/pdf_small.png" style="margin-right:5px;" />Arxius de registre de la CIM-10-MC/SPC</span>'),
    multiple: true,
    position: 'left'
  });

  $('.imgMPC_Directrius').tooltipster({
    animation: 'swing',
    content: $("<span><img src='./img/pdf_small.png' style='margin-right:5px;' />Directrius per a la codificaci&oacute; i l'elaboraci&oacute; d'informes de procediments</span>"),
    multiple: true,
    position: 'left'
  });

  $('.imgMPC_MCTabular').tooltipster({
    animation: 'swing',
    content: $('<span><img src="./img/pdf_small.png" style="margin-right:5px;" />Classificaci&oacute; internacional de malalties, 10a revisi&oacute;, Modificaci&oacute; cl&iacute;nica. Tabular</span>'),
    multiple: true,
    position: 'left'
  });

  $('.imgMPC_MCCercalliure').tooltipster({
    animation: 'swing',
    content: $('<span><img src="./img/pdf_small.png" style="margin-right:5px;" />Classificaci&oacute; internacional de malalties, 10a revisi&oacute;, Modificaci&oacute; cl&iacute;nica. Cerca lliure</span>'),
    multiple: true,
    position: 'left'
  });

  $('.imgMPC_PCSTabulare').tooltipster({
    animation: 'swing',
    content: $('<span><img src="./img/pdf_small.png" style="margin-right:5px;" />Classificaci&oacute; internacional de malalties, 10a revisi&oacute;, Sistema de codificaci&oacute; de procediments</span>'),
    multiple: true,
    position: 'left'
  });

  $('.imgMPC_MCNeo').tooltipster({
    animation: 'swing',
    content: $('<span><img src="./img/pdf_small.png" style="margin-right:5px;" />Classificaci&oacute; internacional de malalties, 10a revisi&oacute;, Modificaci&oacute; cl&iacute;nica. Neopl&agrave;sies</span>'),
    multiple: true,
    position: 'left'
  });

  $('.imgMPC_MCFar').tooltipster({
    content: $('<span><img src="./img/pdf_small.png" style="margin-right:5px;" />Classificaci&oacute; internacional de malalties, 10a revisi&oacute;, Modificaci&oacute; cl&iacute;nica. F&agrave;rmacs</span>'),
    multiple: true,
    position: 'right'
  });

  DEMO.init();

});
